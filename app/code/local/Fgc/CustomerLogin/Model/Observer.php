<?php
class Fgc_CustomerLogin_Model_Observer extends Mage_Core_Model_Abstract {
 
    public function customerLogin(Varien_Event_Observer $observer) {
        $event = $observer->getEvent();
        $event_name = $event->getName();

        switch ($event_name) {
            case "controller_action_predispatch_customer_account_login":
                # code...
                $this->handleCookie();
                break;
            case "controller_action_predispatch_customer_account_loginPost":
                # code...
                break;
            
            default:
                break;
        }
        return;
    }

    function handleCookie() {
        $cookie_domain = Mage::getModel('core/cookie')->getDomain();
        $base_url = Mage::getBaseUrl();

        switch (true) {
            case (preg_match('/shape.customgear.com.au/', $base_url)):
                # code...
                break;
            
            default:
                return;
        }

        $cookie_names = [
            'frontend',
            'frontend_cid',
        ];
        $path = '/';
        $domain = '.customgear.com.au';

        // get all cookies as an array
        $all_cookies_before = Mage::getModel('core/cookie')->get();
        foreach ($cookie_names as $name) {
            $value = Mage::getModel('core/cookie')->get($name);
            // $name is mandatory; other parameters are optional and cen be set as null
            Mage::getModel('core/cookie')->delete($name, $path, $domain/* , $secure, $httponly */);
            // Mage::getModel('core/cookie')->delete($name /*, $path, $domain, $secure, $httponly */);
        }
        $all_cookies_after = Mage::getModel('core/cookie')->get();
    }
}